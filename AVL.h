#pragma once
#include "BST.h"
class AVL :
    public BST
{

public:
	bool displayRotations = true; // rotations to balance a BSTree -> requirement for AVL tree (needs to be balanced!)

	// tells us height of sub tree 
	int height(NumberNode* node);

	// diff between left and right sub trees
	int difference(NumberNode* node);

	// ROTATIONS 

	// return: new parent of subtree
	// parameter: current parent of subtree
	NumberNode* RRrotation(NumberNode* parent);		//		RR

	NumberNode* LLrotation(NumberNode* parent);		//		LL

	NumberNode* LRrotation(NumberNode* parent);		//		LR

	NumberNode* RLrotation(NumberNode* parent);		//		RL 

	// BALANCE FUNCTIONS

	//returns new parent after balancing
	NumberNode* balance(NumberNode* parent);

	// INSERT

	// recursive insert that considers parent a sub tree
	// balamces itself
	// returns new root node
	NumberNode* insertAVL(NumberNode* parent, NumberNode* newNumber);

	// overriding insert from parent
	// AVL.h inherited insert from BST.h, and we could use it
	// BUT we decided to use our own, AVL's version
	void insert(NumberNode* newNumber);
};

