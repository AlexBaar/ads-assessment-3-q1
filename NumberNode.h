#pragma once
#include <iostream>
#include <string>

using namespace std;

class NumberNode
{
public:
	int nodeValue;
	
	NumberNode* leftChild;
	NumberNode* rightChild;

	NumberNode(int nodeValue); // the tree node will only consist of a numerical value (int)
};

