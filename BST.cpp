#include "BST.h"
#include <fstream>
#include "NumberNode.h"

void BST::insert(NumberNode* newNumber)
{ //IF the root is NULL(tree is empty), then make this VALUE the root
    if (root == NULL)
    {
        root = newNumber;
        return; //exit function early, we are done here
    }

    //some pointers to help us navigate the tree to find where to put the new VALUE
    NumberNode* current = root; //current node we're pointing at
    NumberNode* parent = NULL; //parent of current (node visitored last time)

    while (true)//infinite loop
    {
        //lets keep track of where we were before moving down further
        parent = current;
        //LEFT OR RIGHT?!
        //if new VALUE is less then the VALUE at current node, then go down LEFT
        if (newNumber->nodeValue < current->nodeValue)
        {
            // means we go down deeper into tree on left side
            current = current->leftChild;
            //if current is NULL, we just found an empty space to insert our new VALUE :D
            if (current == NULL)
            {
                //done, insert VALUE here
                parent->leftChild = newNumber;
                return; 
            }
        }
        else
        {
            //go down right path
            current = current->rightChild;
            //if current is NULL, insert there
            if (current == NULL)
            {
                parent->rightChild = newNumber;
                return;
            }
        }
    }
}

void BST::show(NumberNode* p)
{
    if (root == NULL)
        return;

    queue<LevelNode>q;

    q.push(LevelNode(root, 0));
    int previousOutputLevel = -1;

    while (q.empty() == false)
    {
        LevelNode node = q.front();
        if (node.level != previousOutputLevel)
        {
            cout << endl;
            cout << node.level << ": ";
            previousOutputLevel = node.level;
        }
        cout << node.number->nodeValue << "  " ;
        q.pop();

        if (node.number->leftChild != NULL)
            q.push(LevelNode(node.number->leftChild, node.level + 1));

        if (node.number->rightChild != NULL)
            q.push(LevelNode(node.number->rightChild, node.level + 1));


    }
    cout << endl;
    
}

