#include "AVL.h"

int AVL::height(NumberNode* node)
{
    int h = 0;

    // break recursion cycle when we get to NULL nodes at the bottom of the tree
    if (node != NULL)
    {
        int leftH = height(node->leftChild);
        int rightH = height(node->rightChild);

        int maxH = max(leftH, rightH);
        h = maxH + 1;
    }
    return h;
}


int AVL::difference(NumberNode* node)
{
    if (node == NULL)
        return 0;

    int leftH = height(node->leftChild);
    int rightH = height(node->rightChild);
    int balanceFactor = leftH - rightH;

    return balanceFactor;
}

NumberNode* AVL::RRrotation(NumberNode* parent)
{
    NumberNode* temp = parent->rightChild;
    parent->rightChild = temp->leftChild;
    temp->leftChild = parent;
    
    return temp;
}

NumberNode* AVL::LLrotation(NumberNode* parent)
{
    NumberNode* temp = parent->leftChild;
    parent->leftChild = temp->rightChild;
    temp->rightChild = parent;
    
    return temp;
}

NumberNode* AVL::LRrotation(NumberNode* parent)
{
    NumberNode* temp = parent->leftChild;
    parent->leftChild = RRrotation(temp);
    
    return LLrotation(parent);
}

NumberNode* AVL::RLrotation(NumberNode* parent)
{
    NumberNode* temp = parent->rightChild;
    parent->rightChild = LLrotation(temp);
    
    return RRrotation(parent);
}

NumberNode* AVL::balance(NumberNode* parent)
{
    int balanceFactor = difference(parent);

    // if BF outside {-1, 0, 1} = IMBALANCED & chose rotation

    if (balanceFactor > 1)
    {
        // if it is >1, it means that the left side of the tree is "heavy"
        // so we chose LL or LR
        if (difference(parent->leftChild) > 0) // left child heavy
        {
            // LL
            parent = LLrotation(parent);
        }
        else
        {
            // LR
            parent = LRrotation(parent);
        }
    }

    else if (balanceFactor < -1)
    {
        // if its < -1 , it means that the right side of the tree is "heavy"
        // so we chose RL or RR
        if (difference(parent->rightChild) > 0)  // right child heavy
        {
            // RL  
            parent = RLrotation(parent);
        }
        else
        {
            // RR
            parent = RRrotation(parent);
        }
    }

    return parent;
}

NumberNode* AVL::insertAVL(NumberNode* parent, NumberNode* newNumber)
{
    if (parent == NULL)
    {
        parent = newNumber;
        return parent;
    }

    // parent not NULL , there is no empty space at this level
    // so we go down one level and insert on either R / L

    if (newNumber->nodeValue < parent->nodeValue)
    {
        parent->leftChild = insertAVL(parent->leftChild, newNumber);
        parent = balance(parent);
    }
    else // so if   (NewNumber->nodeValue > parent->nodeValue)
    {
        parent->rightChild = insertAVL(parent->rightChild, newNumber);
        parent = balance(parent);
    }
}

void AVL::insert(NumberNode* newNumber)
{
    //cout << "Inserting " << newNumber->nodeValue << endl;   we do not need to show this; can uncomment to check if working correctly
    root = insertAVL(root, newNumber);
    
}
