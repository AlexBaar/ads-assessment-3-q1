#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "BST.h"
#include "AVL.h"
using namespace std;

int howManyNumbers;
vector<int> nodeValue;

void writeToAFile()
{
	ofstream  writeFile;
	writeFile.open("input-q1a2.txt");
	writeFile << "8" << endl; 
	writeFile << "14 34 25 92 12 29 51 9" << endl;
	writeFile.close();
}

void readFile()
{
	ifstream readFile;
	readFile.open("input-q1a2.txt");
	
	readFile >> howManyNumbers;
	for (int i = 1; i <= howManyNumbers; i++)
	{
		int temp;
		readFile >> temp;
		nodeValue.push_back(temp);
	}
	readFile.close();
}

void main()
{
	writeToAFile();

	readFile();

	AVL NumberTree;
	NumberTree.displayRotations = true;
	NumberTree.insert(new NumberNode(nodeValue[0]));
	
	NumberTree.insert(new NumberNode(nodeValue[1]));
	
	NumberTree.insert(new NumberNode(nodeValue[2]));
	NumberTree.insert(new NumberNode(nodeValue[3]));
	NumberTree.insert(new NumberNode(nodeValue[4]));
	NumberTree.insert(new NumberNode(nodeValue[5]));
	NumberTree.insert(new NumberNode(nodeValue[6]));
	NumberTree.insert(new NumberNode(nodeValue[7]));

	NumberTree.show(NumberTree.root);
	cout << endl;

	ofstream  writeFile;
	writeFile.open("output-q1-a2.txt");
	for(int i=0; i<howManyNumbers; i++)
	writeFile << "[ "<< NumberTree(NumberTree.root[i]) <<" ] ";
	writeFile.close();


	system("pause");
}