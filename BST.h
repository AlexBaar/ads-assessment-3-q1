#pragma once
#include "NumberNode.h"
#include <queue>

class LevelNode
{
public:
	NumberNode* number;
	int level;
	LevelNode(NumberNode* number, int level)
	{
		this->number = number;
		this->level = level;
	}
};

class BST
{
public:
	NumberNode* root = NULL;
	virtual void insert(NumberNode* newNumber);

	// show
	void show(NumberNode* p);
};